﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Archivo : Componente
    {
        int _tamaño;
        public Archivo(string nombre, int tamaño,Directorio padre) : base(nombre,padre)
        {
            _nombre = nombre;
            _tamaño = tamaño;
            _padre = padre;
        }
        public int Tamaño { get { return _tamaño; } }
        public override void AgregarHijo(Componente c)
        {

        }

        public override IList<Componente> ObtenerHijos()
        {
            return null;
        }

        public override Directorio ObtenerDirectorio(string nombre)
        {
            return null;
        }

        public override Archivo ObtenerArchivo(string nombre)
        {
            return null;
        }

        public override int ObtenerTamaño
        {
            get
            {
                return _tamaño;
            }
        }
    }
}
