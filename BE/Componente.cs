﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public abstract class Componente
    {
        protected string _nombre;
        protected Directorio _padre;
        protected List<Componente> _componentes;
        public Componente(string nombre,Directorio padre)
        {
            _componentes = new List<Componente>();
            _nombre = nombre;
            _padre = padre;
        }

        public Directorio Padre { get { return _padre; } }
        public string Path 
        { 
            get 
            {
                if(_padre != null)
                {
                    return String.Format("{0}\\{1}", _padre.Path, Nombre);
                }
                else
                {
                    return Nombre;
                }              
            }         
        }
        public string Nombre { get { return _nombre; } }
        public abstract void AgregarHijo(Componente c);
        public abstract IList<Componente> ObtenerHijos();
        public abstract int ObtenerTamaño { get; }
        public abstract Directorio ObtenerDirectorio(string nombre);
        public abstract Archivo ObtenerArchivo(string nombre);
    }
}
