﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Directorio : Componente
    {
        private List<Componente> _hijos;
        public Directorio(string nombre, Directorio padre) : base(nombre,padre)
        {
            _nombre = nombre;
            _padre = padre;
            _hijos = new List<Componente>();
        }

        public override void AgregarHijo(Componente c)
        {
            _hijos.Add(c);
            _componentes.Add(c);
        }

        public override IList<Componente> ObtenerHijos()
        {
            return _hijos.ToArray();
        }

        public override Directorio ObtenerDirectorio(string nombre)
        {
            Componente component = _componentes.Where(comp => comp.Nombre.Equals(nombre)).FirstOrDefault();
            if(component is Directorio)
            {
                return (Directorio)component;
            }
            else
            {
                return null;
            }
        }

        public override Archivo ObtenerArchivo(string nombre)
        {
            Componente component = _componentes.Where(comp => comp.Nombre.Equals(nombre)).FirstOrDefault();
            if (component is Archivo)
            {
                return (Archivo)component;
            }
            else
            {
                return null;
            }
        }
        public override int ObtenerTamaño
        {
            get
            {
                int t = 0;

                foreach (var item in _hijos)
                {
                    t += item.ObtenerTamaño;
                }

                return t;
            }

        }

    }
}
