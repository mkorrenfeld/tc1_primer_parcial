﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Usuario
    {
        private string _nombre;
        private string _password;
        private int _id;
        public string Nombre { get { return _nombre; } }
        public string Password { get { return _password; } }
        public int Id { get { return _id; } }
        public Usuario(string nombre,string password)
        {
            _nombre = nombre;
            _password = password;
        }
    }
}
