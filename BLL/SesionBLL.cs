﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using Servicios;
using DAL;
using Servicios.Interpreter;

namespace BLL
{
    public class SesionBLL
    {
        List<Usuario> _usuarios; 

        public SesionBLL()
        {
            _usuarios = new List<Usuario>();
            _usuarios = UsuarioDAL.GetAll();

            //Usuario u = new Usuario("mkorrenfeld", Encriptacion.Hash("mkorrenfeld"));
            //_usuarios.Add(u);
        }
        public void Login(string username, string password)
        {

            if (Singleton.GetInstance != null)
                //     Catch ex As InvalidCommandException
                //   Console.WriteLine("Comando incorrecto...")
                throw new Exception("Ya hay una sesión iniciada");

            var user = _usuarios.Where(usr => usr.Nombre.Equals(username)).FirstOrDefault();
            if (user == null) throw new Exception("Usuario invalido");

            var passEncrip = Encriptacion.Hash(password);
            if (!password.Equals(user.Password.ToString()))
            {
                throw new Exception("Contraseña invalida");
            }                
            else
            {
                Singleton.Login(user);
            }  
        }

        public void Logout()
        {
            if (Singleton.GetInstance == null)
                throw new Exception("No has iniciado sesión");

            Singleton.Logout();
        }

        public string devolverNombreSesion()
        {
            return Singleton.GetInstance.Usuario.Nombre.ToString();
        }

        public bool IsLogged()
        {
             bool l = false;
             if(Singleton.GetInstance != null)
             {
                l = true;
             }
             else 
             {
                l = false;
             }
             return l;
        }
    }
}
