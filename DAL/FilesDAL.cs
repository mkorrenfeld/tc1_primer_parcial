﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
namespace DAL
{
    public static class FilesDAL
    {
        private static string GetConnectionString()
        {
            var cs = new SqlConnectionStringBuilder();
            cs.IntegratedSecurity = true;
            cs.DataSource = "T0003357654\\SQLEXPRESS";
            cs.InitialCatalog = "UaiOS";
            return cs.ConnectionString;
        }

        public static Directorio BuscarComponentePorId(string c)
        {
            var cnn = new SqlConnection(GetConnectionString());
            cnn.Open();

            var cmd2 = new SqlCommand();
            cmd2.Connection = cnn;
            cmd2.CommandText = $@"select * from file_system where id_fs = =@id;";
            cmd2.Parameters.AddWithValue("id", c);

            var reader = cmd2.ExecuteReader();
            Directorio c1;
            while (reader.Read())
            {
                var id = reader.GetInt32(reader.GetOrdinal("id"));
                var nombrefs = reader.GetString(reader.GetOrdinal("nombre"));
                var tipo = reader.GetString(reader.GetOrdinal("tipo"));
                var nombrefspadre = reader.GetString(reader.GetOrdinal("nombre_padre"));
                var tamaño = reader.GetInt32(reader.GetOrdinal("tamaño"));

                
                if (tipo == "Directorio")
                {
                    c1 = new Directorio(nombrefs, BuscarComponentePorId(nombrefspadre));
                    return c1;
                }                    
            }
            return null;
            reader.Close();
        }
        public static List<Componente> GetAll(Usuario u)
        {
            var cnn = new SqlConnection(GetConnectionString());
            cnn.Open();

            var cmd2 = new SqlCommand();
            cmd2.Connection = cnn;
            cmd2.CommandText = $@"select a.id_usuario,a.nombre,c.id_fs,c.nombre,c.tamaño,c.tipo,d.nombre as nombre_padre from usuarios a inner join usuario_filesystem b on a.id_usuario = b.fk_usuario inner join file_system c on c.id_fs = b.fk_fs inner join file_system d on d.id_fs = c.id_fs_padre where id_usuario=@id;";
            cmd2.Parameters.AddWithValue("id", u.Id);

            var reader = cmd2.ExecuteReader();

            List<Componente> lista = new List<Componente>();

            while (reader.Read())
            {

                var id = reader.GetInt32(reader.GetOrdinal("id"));
                var nombrefs = reader.GetString(reader.GetOrdinal("nombre"));
                var tipo = reader.GetString(reader.GetOrdinal("tipo"));
                var nombrefspadre = reader.GetString(reader.GetOrdinal("nombre_padre"));
                var tamaño = reader.GetInt32(reader.GetOrdinal("tamaño"));

                Componente c1;
                if (tipo == "Archivo")
                {                                          
                    c1 = new Archivo(nombrefs, tamaño, BuscarComponentePorId(nombrefspadre));
                    lista.Add(c1);
                }
                else
                {
                    c1 = new Directorio(nombrefs, BuscarComponentePorId(nombrefspadre));
                    lista.Add(c1);
                }

            }
            reader.Close();
            return lista;
        }
    }
}

