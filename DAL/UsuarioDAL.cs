﻿using BE;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class UsuarioDAL
    {
        private static string GetConnectionString()
        {
            var cs = new SqlConnectionStringBuilder();
            cs.IntegratedSecurity = true;
            cs.DataSource = "T0003357654\\SQLEXPRESS";
            cs.InitialCatalog = "UaiOS";
            return cs.ConnectionString;
        }

        public static List<Usuario> GetAll()
        {
            var cnn = new SqlConnection(GetConnectionString());
            cnn.Open();
            var cmd = new SqlCommand();
            cmd.Connection = cnn;

            var sql = $@"select * from usuarios;";

            cmd.CommandText = sql;

            var reader = cmd.ExecuteReader();

            var lista = new List<Usuario>();

            while (reader.Read())
            {
                var nombre = reader.GetString(reader.GetOrdinal("nombre"));
                var password = reader.GetString(reader.GetOrdinal("password"));

                Usuario c = new Usuario(nombre, password);
                lista.Add(c);
            }

            reader.Close();
            cnn.Close();

            return lista;
        }
    }
}
