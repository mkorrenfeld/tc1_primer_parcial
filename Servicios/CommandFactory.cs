﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Servicios.Interpreter;
namespace Servicios
{
    public class CommandFactory
    {
        public Command CreateCommand(string arg)
        {
            string[] command;
            command = arg.Split(' ');

            string nombre;
            string param = "";
            int tamaño = 0;

            if(command.Length == 0 && command.Length > 3)
            {
                throw new Exception("La cantidad de caracteres del comando es incorrecta");   
            }
            else
            {
                nombre = command[0];
                
                
                
                switch (nombre)
                {
                    case "CD":
                        param = command[1];
                        return new ChangeDirectoryCommand(param);
                    case "MD":
                        param = command[1];
                        return new MakeDirectoryCommand(param);
                    case "LS":
                        return new ListCommand();
                    case "MF":
                        param = command[1];
                        tamaño = int.Parse(command[2]);
                        return new MakeFileCommand(param, tamaño);
                    case "DI":
                        return new DisconnectCommand();
                    default:
                        throw new Exception("Comando incorrecto");
                }
            }           
        }
    }
}
