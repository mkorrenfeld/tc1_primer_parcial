﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
namespace Servicios.Interpreter
{
    public class ChangeDirectoryCommand : Command
    {
        public ChangeDirectoryCommand(string param)
        {
            _param = param;
        }

        public override void EjecutarComando(OsContext contexto)
        {
            if (_param == "..")
            {
                contexto.ChangeDirectory(contexto.DirectorioActual.Padre);
            }
            else
            {
                Directorio d = contexto.DirectorioActual.ObtenerDirectorio(_param);

                if (d is null)
                {
                    throw new Exception("No existe el directorio: " + this._param);
                }
                else
                {
                    contexto.ChangeDirectory(d);
                }
            }                
        }
    }
}
