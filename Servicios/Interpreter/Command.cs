﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios.Interpreter
{
    public abstract class Command
    {
        protected string _param;

        public Command()
        {

        }

        public Command(string param)
        {
            _param = param;
        }

        public abstract void EjecutarComando(OsContext contexto);
    }
}
