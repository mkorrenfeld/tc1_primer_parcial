﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
namespace Servicios.Interpreter
{
    public class DisconnectCommand : Command
    {
        
        public override void EjecutarComando(OsContext contexto)
        {
            Singleton.Logout();
        }      
    }
}
