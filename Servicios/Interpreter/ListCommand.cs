﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
namespace Servicios.Interpreter
{
    public class ListCommand : Command
    {
        public override void EjecutarComando(OsContext contexto)
        {
            Console.WriteLine("Se lista el contenido de {0}:\n",contexto.DirectorioActual.Path);
            
            foreach (var componente in contexto.DirectorioActual.ObtenerHijos())
            {
                Console.WriteLine("- {0} {1} {2}kb", componente.GetType().Name, componente.Nombre,componente.ObtenerTamaño);
            }

            Console.WriteLine("");
            Console.WriteLine("Contenido total de: {0} elementos -- tamaño total: {1}",contexto.DirectorioActual.ObtenerHijos().Count, contexto.DirectorioActual.ObtenerTamaño);
        }
    }
}
