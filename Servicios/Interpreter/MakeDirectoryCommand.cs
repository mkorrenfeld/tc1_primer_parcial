﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
namespace Servicios.Interpreter
{
    public class MakeDirectoryCommand : Command
    {
        public MakeDirectoryCommand(string param)
        {
            _param = param;            
        }
        public override void EjecutarComando(OsContext contexto)
        {
            if(contexto.DirectorioActual.ObtenerDirectorio(_param) == null)
            {
                Directorio d = new Directorio(_param, contexto.DirectorioActual);
                contexto.DirectorioActual.AgregarHijo(d);
            }
            else
            {
                throw new Exception("El directorio ya existe");
            }           
        }
    }
}
