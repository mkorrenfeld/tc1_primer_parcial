﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace Servicios.Interpreter
{
    public class MakeFileCommand : Command
    {
        string _nombre;
        int _tamaño;

        public MakeFileCommand(string nombre, int tamaño)
        {
            _nombre = nombre;
            _tamaño = tamaño;
        }

        public override void EjecutarComando(OsContext contexto)
        {
            if(contexto.DirectorioActual.ObtenerDirectorio(_nombre) is null)
            {
                Archivo a = new Archivo(_nombre, _tamaño, contexto.DirectorioActual);
                contexto.DirectorioActual.AgregarHijo(a);
            }
            else
            {
                throw new Exception("El archivo ya existe");
            }
        }
    }
}
