﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;
namespace Servicios.Interpreter
{
    public class OsContext
    {
        Directorio _directorioActual;
        List<Componente> _fileSystem;

        public Directorio DirectorioActual { get { return _directorioActual; } }
        public OsContext(string proot)
        {
            _fileSystem = new List<Componente>();
            Directorio drive = new Directorio("C:", null);
            Directorio root = new Directorio(proot, drive);
            _fileSystem.Add(root);
            _directorioActual = root;
        }

        public void MakeDirectory(Directorio dir)
        {
            _directorioActual.AgregarHijo(dir);
        }
        public void ChangeDirectory(Directorio dir)
        {
            if(dir.Nombre == "C:")
            {
                _directorioActual = _directorioActual;
            }
            else
            {
                _directorioActual = dir;
            }    
        }
        public void MakeFile(Archivo arch)
        {
            _directorioActual.AgregarHijo(arch);
        }

    }
}
