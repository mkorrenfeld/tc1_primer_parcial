﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public interface ISujetoSesion
    {
        void RegistrarObservador(IObservadorSesion observador);
        void QuitarObservador(IObservadorSesion observador);
        void Notificar();
    }
}
