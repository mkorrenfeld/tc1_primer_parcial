﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using Servicios.Interpreter;
namespace Servicios
{
    public class Singleton 
    {

        private static object _lock = new Object();
        private static Singleton _session;

        public Usuario Usuario { get; set; }
        public DateTime FechaInicio { get; set; }

        public static Singleton GetInstance
        {
            get
            {
                if (_session == null) { return null; }
                else { return _session; };            
            }
        }

        public static void Login(Usuario usuario)
        {

            lock (_lock)
            {
                if (_session == null)
                {
                    _session = new Singleton();
                    _session.Usuario = usuario;
                    _session.FechaInicio = DateTime.Now;
                }
                else
                {
                    throw new Exception("Sesión ya iniciada");
                }
            }
        }

        public static void Logout()
        {
            lock (_lock)
            {
                if (_session != null)
                {
                    _session = null;
                }
                else
                {
                    throw new Exception("Sesión no iniciada");
                }
            }
        }

        private Singleton()
        {

        }


    }

}
