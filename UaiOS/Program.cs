﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using BLL;
using Servicios.Interpreter;
using Servicios;

namespace UaiOS
{
    class Program
    {
        public static SesionBLL _sesioBLL = new SesionBLL();
        
        static void Main(string[] args)
        {
            OsContext _osContext;           
            Command _Command;
            
            string _comandoIngresado;

            do
            {
                try
                {
                    Login();                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al iniciar sesión: " + ex.Message);
                }
            } while (!_sesioBLL.IsLogged());


            void Login()
            {
                Console.WriteLine("Hola!, introducir nombre de usuario:");
                var _usuarioIngreso = Console.ReadLine();

                Console.WriteLine("introducir su contraseña:");
                var _passwordIngreso = Console.ReadLine();

                _sesioBLL.Login(_usuarioIngreso, _passwordIngreso);
                Console.WriteLine("Bienvenido " + _sesioBLL.devolverNombreSesion() + "!\n");
                
            }

            _osContext = new OsContext(_sesioBLL.devolverNombreSesion());

            do
            {
                try
                {
                    SolicitarComando();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al ejecutar el comando: " + ex.Message);
                }
            } while (_sesioBLL.IsLogged());

            void Logout()
            {
                Console.WriteLine("Hasta luego " + _sesioBLL.devolverNombreSesion() + "!\n");
                _sesioBLL.Logout();
                Console.ReadKey();
            }

            void SolicitarComando()
            {
                Console.WriteLine( _osContext.DirectorioActual.Path +">");
                _comandoIngresado = Console.ReadLine();

                CommandFactory cf = new CommandFactory();
                _Command = cf.CreateCommand(_comandoIngresado);
                _Command.EjecutarComando(_osContext);
            }          

            Console.ReadKey();

        }
    }
}
